FROM igwn/lalsuite-dev:el7

LABEL name="LALInference master EL7" \
      maintainer="James Alexander CLark <james.clark@ligo.org>" \
      date="20200710" \
      support="Experimental Platform"

# Setup directories for binding
RUN mkdir -p /cvmfs /hdfs /gpfs /ceph /hadoop /etc/condor

RUN yum upgrade -y && \
    yum clean all && \
    rm -rf /var/cache/yum

RUN git clone https://git.ligo.org/lscsoft/lalsuite.git && \ 
      cd lalsuite && \
      ./00boot  && \
      ./configure --prefix /opt/lscsoft/lalsuite --enable-swig-python --disable-lalstochastic --disable-laldetchar && \
      make -j && \
      make install && \
      cd / && \
      rm -rf lalsuite

RUN ["/bin/bash", "-c", "source /opt/lscsoft/lalsuite/etc/lalsuiterc"]

COPY docker-entrypoint.sh /usr/local/bin/
RUN ln -s usr/local/bin/docker-entrypoint.sh / # backwards compat
ENTRYPOINT ["docker-entrypoint.sh"]
